FROM python:3.5-slim

LABEL Maintainer="Kyle Patience <kylepatience@gmail.com>"

WORKDIR /app

ADD . /app

RUN pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 80

CMD ["python", "app.py"]
