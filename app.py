from flask import Flask
import requests
from coordinate import Coordinate
import json

app = Flask(__name__)

# ========= CONTROLLER LOGIC ==============
@app.route("/find-missing-trees-for-orchard/<orchard_id>", methods=["GET"])
def get_missing_trees_for_orchard(orchard_id):
    missing_trees = missing_trees_for_orchard(orchard_id)

    body = list(map(display_coordinate, missing_trees))
    response = json.dumps({"missing_trees": body})

    return response


def display_coordinate(coordinate):
    return {"lat": coordinate.latitude,
            "lng": coordinate.longitude}

# ========================================


# ========= SERVICE LOGIC ================
def missing_trees_for_orchard(orchard_id):
    unsorted_trees = tree_coordinates_for_orchard(orchard_id)
    trees = sort_trees_by_latitude(unsorted_trees)

    # Setting the precedence of displacement between trees in the orchard given the assumption of trees are evenly
    # distributed. (Room for improvement)
    initial_displacement = trees[1].subtract(trees[0])

    return identify_missing_trees(trees, initial_displacement)


# Sort random tree coordinates by latitude
def sort_trees_by_latitude(trees):
    sorted_trees = sorted(trees, key=lambda x: x.latitude)
    return sorted_trees


def identify_missing_trees(og_trees, displacement):
    trees = og_trees.copy()
    confirmed_missing_trees = []
    while len(trees) > 1:
        # Calculate the supposed location of the next tree given a displacement
        supposed_location = trees[0].add(displacement)
        existing_tree = find_trees_within_range(trees, supposed_location, displacement)

        if not existing_tree:
            # Given a tree does not exist, we calculate the next supposed location off the current supposed location.
            # If a tree is found on the next supposed location we can then confirm that there was a gap in the orchard.
            next_supposed_location = supposed_location.add(displacement)
            next_existing_tree = find_trees_within_range(trees, next_supposed_location, displacement)

            if next_existing_tree:
                confirmed_missing_trees.append(supposed_location)

        # Instead of using the entire list of trees when searching for existing trees, rather remove the tree we already
        # processed to improve iteration time.
        trees = trees[1:]
    return confirmed_missing_trees


# Iterates through list of trees to find one that falls within the supposed location. Since the list is sorted,
# if a tree is found, it's found sooner rather than later. If not found, the whole list is iterated.
def find_trees_within_range(trees, supposed_coordinate, displacement):
    for actual_tree in trees:
        if equal_within_range(actual_tree.latitude, supposed_coordinate.latitude, displacement.latitude):
            if equal_within_range(actual_tree.longitude, supposed_coordinate.longitude, displacement.longitude):
                return True
    return False


# A fuzzy equals implementation to cater for coordinates
def equal_within_range(actual_tree, supposed_coordinate_unit, displacement):
    # 0.9 chosen to increase capture area but ignore the base tree itself
    window_start = supposed_coordinate_unit - (displacement * 0.9)
    window_end = supposed_coordinate_unit + (displacement * 0.9)
    if (actual_tree >= min(window_start, window_end)) and (actual_tree <= max(window_start, window_end)):
        return True
    else:
        return False
# ====================================================


# ========= COULD BE PLACED IN A CLIENT ==============
def tree_coordinates_for_orchard(orchard_id):
    tree_coordinates = []
    for surveyed_tree in get_latest_survey_data_for_orchard(orchard_id)['results']:
        tree_coordinates.append(Coordinate(surveyed_tree['latitude'], surveyed_tree['longitude']))
    return tree_coordinates


def get_latest_survey_data_for_orchard(orchard_id):
    # Should be added in ENV variables
    url = 'XXX'
    auth_token = 'XXX'
    response = requests.get(
        url,
        params={'survey__orchard_id': orchard_id},
        headers={'Authorization': auth_token},
    )
    return response.json()

# ====================================================


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
