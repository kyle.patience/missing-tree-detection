class Coordinate:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def subtract(self, subtrahend_coordinate):
        return Coordinate((self.latitude - subtrahend_coordinate.latitude),
                          (self.longitude - subtrahend_coordinate.longitude))

    def add(self, addend_coordinate):
        return Coordinate((self.latitude + addend_coordinate.latitude),
                          (self.longitude + addend_coordinate.longitude))

    def multiply(self, factor_coordinate):
        return Coordinate((self.latitude * factor_coordinate.latitude),
                          (self.longitude * factor_coordinate.longitude))

    def divide(self, divisor_coordinate):
        return Coordinate((self.latitude / divisor_coordinate.latitude),
                          (self.longitude / divisor_coordinate.longitude))

    def divide_by_number(self, number):
        return Coordinate((self.latitude / number),
                          (self.longitude / number))
